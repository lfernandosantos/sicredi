//
//  ServiceManagerMock.swift
//  SicredEventsTests
//
//  Created by Fernando on 30/12/20.
//

@testable import SicredEvents
import Foundation

final class RequestManagerMock: RequestManagerProtocol {
    
    let shouldFail: Bool
    let hasDecodeError: Bool
    let eventsJson: [[String: Any]] = [
        ["people": [], "date": 1609368638646, "description": "1234 description", "image": "1234", "longitude": -1234.1234, "latitude": -1234.1234, "price": 10.99,"title": "Title Test","id": "1"],
        ["people": [], "date": 1609368638646, "description": "1234 description", "image": "1234", "longitude": -1234.1234, "latitude": -1234.1234, "price": 11.99,"title": "Title Test","id": "2"],
        ["people": [], "date": 1609368638646, "description": "1234 description", "image": "1234", "longitude": -1234.1234, "latitude": -1234.1234, "price": 10.99,"title": "Title Test","id": "3"]
    ]
    
    init(shouldFail: Bool, hasDecodeError: Bool = false) {
        self.shouldFail = shouldFail
        self.hasDecodeError = hasDecodeError
    }
    
    func request(endpoint: Endpoint, body: Data?, completion: @escaping (Result<Data?, Error>) -> Void) {
        let data = try? JSONSerialization.data(withJSONObject: eventsJson, options: .prettyPrinted)
        
        if shouldFail {
            if hasDecodeError {
                completion(.success(Data()))
            } else {
                completion(.failure(ResultError.invalidURL))
            }
        } else {
            completion(.success(data))
        }
    }
}
