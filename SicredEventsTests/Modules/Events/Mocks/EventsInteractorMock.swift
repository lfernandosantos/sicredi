//
//  EventsInteractorMock.swift
//  SicredEventsTests
//
//  Created by Fernando on 27/12/20.
//

@testable import SicredEvents


final class EventsInteractorMock: EventsInteractorProtocol {
    var delegate: EventsInteractorDelegate?
    private let shouldFail: Bool
    
    init(shouldFail: Bool) {
        self.shouldFail = shouldFail
    }
    
    func fetchEvents() {
        if shouldFail {
            delegate?.didFail(error: ResultError.decodeError)
        } else {
            delegate?.didFetch(events: EventsMock.make())
        }
    }
}
