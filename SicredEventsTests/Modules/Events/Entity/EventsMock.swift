//
//  EventsMock.swift
//  SicredEventsTests
//
//  Created by Fernando on 27/12/20.
//

@testable import SicredEvents

struct EventsMock {
    static func make() -> [Event] {
        return [ Event(people: [],
                       date: 1609368638646,
                       description: "1234 description",
                       image: "1234",
                       longitude: -1234.1234,
                       latitude: -1234.1234,
                       price: 10.99,
                       title: "Title Test",
                       id: "1"),
                 Event(people: [],
                       date: 1609368638646,
                       description: "1234 description",
                       image: "1234",
                       longitude: -1234.1234,
                       latitude: -1234.1234,
                       price: 11.99,
                       title: "Title Test",
                       id: "2"),
                 Event(people: [],
                       date: 1609368638646,
                       description: "1234 description",
                       image: "1234",
                       longitude: -1234.1234,
                       latitude: -1234.1234,
                       price: 10.99,
                       title: "Title Test",
                       id: "3")]
    }
}

