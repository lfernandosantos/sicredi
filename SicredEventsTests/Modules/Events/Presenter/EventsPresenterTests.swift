//
//  EventsPresenterTests.swift
//  SicredEventsTests
//
//  Created by Fernando on 27/12/20.
//

import XCTest
@testable import SicredEvents

class EventsPresenterTests: XCTestCase {

    private var presenter: EventsPresenter!
    
    func testFetchEvents() {
        let delegate = EventsPresenterDelegateMock()
        let interactor = EventsInteractorMock(shouldFail: false)
        presenter = EventsPresenter(interactor:  interactor, router: EventsRouter())
        presenter.delegate = delegate
        interactor.delegate = presenter
        
        presenter.viewDidLoad()
        
        XCTAssertEqual(delegate.events, EventViewModelMock.make())
    }
    
    func testFetchFailEvents() {
        let delegate = EventsPresenterDelegateMock()
        let interactor = EventsInteractorMock(shouldFail: true)
        presenter = EventsPresenter(interactor:  interactor, router: EventsRouter())
        presenter.delegate = delegate
        interactor.delegate = presenter
        
        presenter.viewDidLoad()
        
        XCTAssertNil(delegate.events)
    }

}


final class EventsPresenterDelegateMock: EventsViewProtocol {
    public var events: [EventViewModel]? = nil

    func setup(events: [EventViewModel]) {
        self.events = events
    }
}
