//
//  EventsInteractorTests.swift
//  SicredEventsTests
//
//  Created by Fernando on 30/12/20.
//

import XCTest
@testable import SicredEvents

class EventsInteractorTests: XCTestCase {

    private var interactor: EventsInteractor!
    private var events: [Event]? = nil
    private var error: ResultError? = nil
    
    override func setUp() {
        events = nil
        error = nil
    }
    
    func testFetchEvents() {
        let requestMock = RequestManagerMock(shouldFail: false)
        interactor = EventsInteractor(requestManager: requestMock)
        interactor.delegate = self
        interactor.fetchEvents()
        
        XCTAssertNil(self.error)
        XCTAssertEqual(self.events, EventsMock.make())
    }
    
    func testFetchFailEvents() {
        let requestMock = RequestManagerMock(shouldFail: true)
        interactor = EventsInteractor(requestManager: requestMock)
        interactor.delegate = self
        interactor.fetchEvents()
        
        XCTAssertNotNil(self.error)
        XCTAssertNil(self.events)
    }
    
    func testFetchFailDecode() {
        let requestMock = RequestManagerMock(shouldFail: true, hasDecodeError: true)
        interactor = EventsInteractor(requestManager: requestMock)
        interactor.delegate = self
        interactor.fetchEvents()
        
        XCTAssertNotNil(self.error)
        XCTAssertEqual(self.error, ResultError.decodeError)
        XCTAssertNil(self.events)
    }
}

// MARK: - EventsInteractorDelegate

extension EventsInteractorTests: EventsInteractorDelegate {
    func didFetch(events: [Event]) {
        self.events = events
        self.error = nil
    }
    
    func didFail(error: Error) {
        self.events = nil
        self.error = error as? ResultError
    }
}
