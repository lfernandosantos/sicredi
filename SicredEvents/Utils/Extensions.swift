//
//  String+Extension.swift
//  SicredEvents
//
//  Created by Fernando on 30/12/20.
//
import Foundation
import UIKit

// MARK: - Double

extension Double {
    var formattedCurrencyValue: String {
        return String(format: "R$%.02f", self)
    }
}

// MARK: - Int

extension Int {
    var milisecondsToDateValue: String {
        let date = Date(timeIntervalSince1970: TimeInterval(self) / 1000)
        let nameFormatter = DateFormatter()
        nameFormatter.dateFormat = "dd - MMM"
        return nameFormatter.string(from: date)
    }
}

// MARK: - UIImageView

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
