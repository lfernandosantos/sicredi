//
//  EventCell.swift
//  SicredEvents
//
//  Created by Fernando on 30/12/20.
//

import UIKit

final class EventCell: UITableViewCell {

    @IBOutlet private weak var backgroundCardView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    

    func setUp(title: String, address: String, price: String) {
        backgroundCardView.layer.cornerRadius = 12
        backgroundCardView.layer.masksToBounds = false
        backgroundCardView.layer.shadowColor = UIColor.black.cgColor
        backgroundCardView.layer.shadowOpacity = 1.0
        backgroundCardView.layer.shadowOffset = CGSize(width: -1, height: 1)
        backgroundCardView.layer.shadowRadius = 1.5
        
        self.selectionStyle = .none
        
        titleLabel.text = title
        dateLabel.text = address
        priceLabel.text = price
    }
    
}
