//
//  EventsViewController.swift
//  SicredEvents
//
//  Created by Fernando on 19/12/20.
//

import UIKit

protocol EventsViewProtocol: AnyObject {
    func setup(events: [EventViewModel])
}

final class EventsViewController: UIViewController {

    private var presenter: EventsPresenterProtocol
    private var events: [EventViewModel] = []
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet private weak var tableView: UITableView!
    
    init(presenter: EventsPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: "EventsViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "eventCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        presenter.delegate = self
        presenter.viewDidLoad()
    }
}

// MARK: - EventsViewProtocol

extension EventsViewController: EventsViewProtocol {
    
    func setup(events: [EventViewModel]) {
        self.events = events
        tableView.reloadData()
    }

}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension EventsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let eventCell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as? EventCell else {
            return UITableViewCell()
        }
        
        let event = events[indexPath.row]
        eventCell.setUp(title: event.title, address: event.date, price: event.price)
        return eventCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventID = events[indexPath.row].id
        presenter.didSelectEvent(id: eventID)
    }
}
