//
//  EventsInteractor.swift
//  SicredEvents
//
//  Created by Fernando on 19/12/20.
//

import Foundation

protocol EventsInteractorProtocol {
    var delegate: EventsInteractorDelegate? { get set }
    
    func fetchEvents()
}

protocol EventsInteractorDelegate: AnyObject {
    func didFetch(events: [Event])
    func didFail(error: Error)
}

final class EventsInteractor: EventsInteractorProtocol {
    
    private let requestManager: RequestManagerProtocol
    
    weak var delegate: EventsInteractorDelegate?
    
    init(requestManager: RequestManagerProtocol = RequestManager()) {
        self.requestManager = requestManager
    }
    
    func fetchEvents() {
        requestManager.request(endpoint: .events) { (result) in
            switch result {
            case .success(let data):
                if let data = data, let events = try? JSONDecoder().decode([Event].self, from: data) {
                    self.delegate?.didFetch(events: events)
                } else {
                    self.delegate?.didFail(error: ResultError.decodeError)
                }
                
            case .failure(let error):
                self.delegate?.didFail(error: error)
            }
        }
    }
}
