//
//  EventsRouter.swift
//  SicredEvents
//
//  Created by Fernando on 19/12/20.
//

import UIKit


protocol EventsRouterProtocol {
    func create() -> UIViewController
    func openEventDetails(id: String)
}

final class EventsRouter: EventsRouterProtocol {
    
    private var view: UIViewController?
    
    func create() -> UIViewController {
        let interactor = EventsInteractor()
        let presenter = EventsPresenter(interactor: interactor, router: self)
        interactor.delegate = presenter
        let view = EventsViewController(presenter: presenter)
        presenter.delegate = view
        view.modalPresentationStyle = .fullScreen
        
        self.view = view
        return view
    }
    
    func openEventDetails(id: String) {
        let eventDetailsView = EventDetailsRouter().create(eventID: id)
        view?.present(eventDetailsView, animated: true, completion: nil)
    }
}
