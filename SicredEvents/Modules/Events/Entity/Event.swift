//
//  Event.swift
//  SicredEvents
//
//  Created by Fernando on 19/12/20.
//

struct Event: Codable, Equatable {
    var people: [String]
    var date: Int
    var description: String
    var image: String
    var longitude: Double
    var latitude: Double
    var price: Double
    var title: String
    var id: String
}
