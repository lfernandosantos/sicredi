//
//  EventsPresenter.swift
//  SicredEvents
//
//  Created by Fernando on 19/12/20.
//

protocol EventsPresenterProtocol {
    var delegate: EventsViewProtocol? { get set }
    
    func viewDidLoad()
    func didSelectEvent(id: String)
}

final class EventsPresenter: EventsPresenterProtocol {
    
    private let interactor: EventsInteractorProtocol
    private let router: EventsRouterProtocol
    
    weak var delegate: EventsViewProtocol?
    
    init(interactor: EventsInteractorProtocol,
         router: EventsRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        interactor.fetchEvents()
    }
    
    func didSelectEvent(id: String) {
        router.openEventDetails(id: id)
    }
    
    private func makeEventsViewModel(events: [Event]) -> [EventViewModel] {
        return events.compactMap { EventViewModel(id: $0.id,
                                                  title: $0.title,
                                                  date: $0.date.milisecondsToDateValue,
                                                  price: $0.price.formattedCurrencyValue) }
    }
}


extension EventsPresenter: EventsInteractorDelegate {
    func didFetch(events: [Event]) {
        delegate?.setup(events: self.makeEventsViewModel(events: events))
    }
    
    func didFail(error: Error) {
        print(error.localizedDescription)
    }
}
