//
//  EventViewModel.swift
//  SicredEvents
//
//  Created by Fernando on 30/12/20.
//


struct EventViewModel: Equatable {
    let id: String
    let title: String
    let date: String
    let price: String
}
