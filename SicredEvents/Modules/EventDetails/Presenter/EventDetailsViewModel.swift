//
//  EventDetailsViewModel.swift
//  SicredEvents
//
//  Created by Fernando on 30/12/20.
//

struct EventDetailsViewModel {
    let id: String
    let title: String
    let image: String
    let date: String
    let price: String
    let description: String
    let address: String
    let isEmpty: Bool
}
