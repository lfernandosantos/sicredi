//
//  EventDetailsPresenter.swift
//  SicredEvents
//
//  Created by Fernando on 20/12/20.
//

protocol EventDetailsPresenterProtocol: AnyObject {
    var delegate: EventDetailsViewProtocol? { get set }
    
    func viewDidLoad()
    func didSelectedCheckinButton()
    func didSelectedAddressButton()
    func didSelectedShareButton()
}

final class EventDetailsPresenter: EventDetailsPresenterProtocol {
    
    private let interactor: EventDetailsInteractorProtocol
    private let eventID: String
    private let router: EventDetailsRouterProtocol
    private let localeService: LocaleServiceProtocol
    private var event: Event?
    private var eventDetailViewModel: EventDetailsViewModel?
    
    var delegate: EventDetailsViewProtocol?
    
    
    init(interactor: EventDetailsInteractorProtocol,
         eventID: String,
         router: EventDetailsRouterProtocol,
         localeService: LocaleServiceProtocol = LocaleService()) {
        self.interactor = interactor
        self.eventID = eventID
        self.router = router
        self.localeService = localeService
    }
    
    func viewDidLoad() {
        interactor.fetchEventDetails(id: eventID)
    }
    
    func didSelectedCheckinButton() {
        interactor.checkinEvent(id: eventID, name: "Fernando", email: "Luiz")
    }
    
    func didSelectedAddressButton() {
        guard let event = event else { return }
        router.openMap(name: event.title, latitude: event.latitude, longitude: event.longitude)
    }
    
    func didSelectedShareButton() {
        guard let event = eventDetailViewModel else { return }
        router.shareEvent(title: event.title, date: event.date)
    }
    
    private func setUpView(event: Event) {
        localeService.getAddress(latitude: event.latitude, longitude: event.longitude) { [weak self] (address) in
            
            let localeAddres = address ?? "Clique para ver no mapa"
            let viewModel = EventDetailsViewModel(id: event.id,
                                                  title: event.title,
                                                  image: event.image,
                                                  date: event.date.milisecondsToDateValue,
                                                  price: event.price.formattedCurrencyValue,
                                                  description: event.description,
                                                  address: localeAddres,
                                                  isEmpty: event.people.count == 0)
            self?.eventDetailViewModel = viewModel
            self?.delegate?.setup(event: viewModel)
        }
    }
}

extension EventDetailsPresenter: EventDetailsInteractorDelegate {
    func didCheckin(isSuccess: Bool) {
        delegate?.updatedCheckinButton(isSuccess)
    }
    
    func didFetch(event: Event) {
        self.event = event
        setUpView(event: event)
    }
    
    func didFail(error: Error) {
        print(error.localizedDescription)
    }
}
