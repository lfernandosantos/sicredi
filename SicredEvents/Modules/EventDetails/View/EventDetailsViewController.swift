//
//  EventDetailsViewController.swift
//  SicredEvents
//
//  Created by Fernando on 20/12/20.
//

import UIKit
import CoreLocation
import Contacts

protocol EventDetailsViewProtocol {
    func setPresenter(_ presenter: EventDetailsPresenterProtocol)
    func setup(event: EventDetailsViewModel)
    func updatedCheckinButton(_ isSelected: Bool)
}

final class EventDetailsViewController: UIViewController {
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UITextView!
    @IBOutlet private weak var shareButton: UIButton!
    @IBOutlet private weak var confirmButton: UIButton!
    @IBOutlet private weak var peopleButton: UIButton!
    @IBOutlet private weak var backgroundCardDataView: UIView!
    @IBOutlet private weak var backgroundCardPriceView: UIView!
    @IBOutlet private weak var checkinButton: UIButton!
    
    private var presenter: EventDetailsPresenterProtocol?
    
    
    init() {
        super.init(nibName: "EventDetailsViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapInside = UITapGestureRecognizer(target: self, action: #selector(self.addressButtonAction(sender:)))
        addressLabel.addGestureRecognizer(tapInside)
        
        shareButton.layer.cornerRadius = 3
        peopleButton.layer.cornerRadius = 3
        confirmButton.layer.cornerRadius = 3
        confirmButton.isUserInteractionEnabled = true
        
        backgroundCardDataView.layer.cornerRadius = backgroundCardDataView.frame.height / 2
        
        backgroundCardPriceView.layer.cornerRadius = backgroundCardPriceView.frame.height / 2
        
        checkinButton.layer.cornerRadius = checkinButton.frame.height / 2
        
        presenter?.viewDidLoad()
    }
    
    @IBAction private func confirmButtonAction(sender: Any?) {
        presenter?.didSelectedCheckinButton()
        self.updatedCheckinButton(!confirmButton.isSelected)
    }
    
    @IBAction private func addressButtonAction(sender: Any?) {
        presenter?.didSelectedAddressButton()
    }
    
    @IBAction private func shareButtonAction(sender: Any?) {
        presenter?.didSelectedShareButton()
    }
}

extension EventDetailsViewController: EventDetailsViewProtocol {
    func setup(event: EventDetailsViewModel) {

        if let imageURL = URL(string: event.image) {
            imageView.load(url: imageURL)
            imageView.layer.cornerRadius = 4
        }
        titleLabel.text = event.title
        priceLabel.text = event.price
        dateLabel.text = event.date
        descriptionLabel.text = event.description
        
        

        peopleButton.isSelected = !event.isEmpty
        
        let attrs = [NSAttributedString.Key.underlineStyle : 1]
        let attributedString = NSMutableAttributedString(string: event.address, attributes: attrs)
        self.addressLabel.attributedText = attributedString
    }
    
    func setPresenter(_ presenter: EventDetailsPresenterProtocol) {
        self.presenter = presenter
    }
    
    func updatedCheckinButton(_ isSelected: Bool) {
        confirmButton.isSelected = isSelected
    }
    
}
