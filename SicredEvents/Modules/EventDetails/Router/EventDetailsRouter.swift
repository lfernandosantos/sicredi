//
//  EventsDetailsRouter.swift
//  SicredEvents
//
//  Created by Fernando on 20/12/20.
//

import UIKit

protocol EventDetailsRouterProtocol {
    func shareEvent(title: String, date: String)
    func openMap(name: String, latitude: Double, longitude: Double)
}

final class EventDetailsRouter: EventDetailsRouterProtocol {
    
    weak var view: UIViewController?
    
    func create(eventID: String) -> UIViewController {
        
        let interactor = EventDetailsInteractor()
        let presenter = EventDetailsPresenter(interactor: interactor, eventID: eventID, router: self)
        interactor.delegate = presenter
        
        let view = EventDetailsViewController()
        view.setPresenter(presenter)
        presenter.delegate = view
        self.view = view
        return view
    }
    
    func shareEvent(title: String, date: String) {
        let messageDate = "Data: \(date)"
        let objectsToShare = [title, messageDate]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
        self.view?.present(activityVC, animated: true, completion: nil)
    }
    
    func openMap(name: String, latitude: Double, longitude: Double) {
        LocaleService().openMaps(at: name, lat: latitude, lon: longitude)
    }
}
