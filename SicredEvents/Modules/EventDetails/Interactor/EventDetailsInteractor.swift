//
//  EventDetailsInteractor.swift
//  SicredEvents
//
//  Created by Fernando on 20/12/20.
//

import Foundation

protocol EventDetailsInteractorProtocol {
    var delegate: EventDetailsInteractorDelegate? { get set }
    
    func fetchEventDetails(id: String)
    func checkinEvent(id: String, name: String, email: String)
}

protocol EventDetailsInteractorDelegate: AnyObject {
    func didCheckin(isSuccess: Bool)
    func didFetch(event: Event)
    func didFail(error: Error)
}

final class EventDetailsInteractor: EventDetailsInteractorProtocol {
    
    private let requestManager: RequestManagerProtocol
    weak var delegate: EventDetailsInteractorDelegate?
    
    init(requestManager: RequestManagerProtocol = RequestManager()) {
        self.requestManager = requestManager
    }
    
    func fetchEventDetails(id: String) {
        requestManager.request(endpoint: .eventDetails(id: id)) { [weak self] (result) in
            switch result {
            case .success(let data):
                if let data = data, let event = try? JSONDecoder().decode(Event.self, from: data) {
                    self?.delegate?.didFetch(event: event)
                } else {
                    self?.delegate?.didFail(error: ResultError.decodeError)
                }
                
            case .failure(let error):
                self?.delegate?.didFail(error: error)
            }
        }
    }
    
    func checkinEvent(id: String, name: String, email: String) {
        
        let chekinEntity = CheckinEntity(eventId: id, name: name, email: email)
        let body = try? JSONEncoder().encode(chekinEntity)
        requestManager.request(endpoint: .checkin, body: body) { [weak self] (result) in
            switch result {
            case .success:
                self?.delegate?.didCheckin(isSuccess: true)
            case .failure:
                self?.delegate?.didCheckin(isSuccess: false)
            }
        }
    }
}

struct CheckinEntity: Codable {
    var eventId: String
    var name: String
    var email: String
}
