//
//  Endpoint.swift
//  SicredEvents
//
//  Created by Fernando on 19/12/20.
//

import Foundation

enum Endpoint {
    case events
    case eventDetails(id: String)
    case checkin
}

extension Endpoint {
    
    var method: String {
        switch self {
        case .events, .eventDetails:
            return "GET"
        case .checkin:
            return "POST"
        }
    }
    
    var path: String {
        switch self {
        case .events:
            return "events"
        case .eventDetails(let id):
            return "events/\(id)"
        case .checkin:
            return "checkin"
        }
    }
}
