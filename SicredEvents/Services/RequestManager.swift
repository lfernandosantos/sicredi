//
//  RequestManager.swift
//  SicredEvents
//
//  Created by Fernando on 19/12/20.
//

import Foundation

protocol RequestManagerProtocol {
    func request(endpoint: Endpoint, body: Data?, completion: @escaping (Result<Data?,Error>) -> Void)
}

extension RequestManagerProtocol {
    func request(endpoint: Endpoint, body: Data? = nil, completion: @escaping (Result<Data?,Error>) -> Void) {
        self.request(endpoint: endpoint, body: body, completion: completion)
    }
}

final class RequestManager: RequestManagerProtocol {
    
    private let session: URLSession = URLSession(configuration: .default)
    
    func request(endpoint: Endpoint, body: Data? = nil, completion: @escaping (Result<Data?,Error>) -> Void) {
        guard let url = URL(string: Constants.BASE_URL + endpoint.path) else {
            completion(.failure(ResultError.invalidURL))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = endpoint.method
        urlRequest.httpBody = body
        
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.success(data))
                }
            }
        }
        
        task.resume()
    }
}

// MARK: - ResultError

enum ResultError: Error, Equatable {
    case generic
    case invalidURL
    case decodeError
}

