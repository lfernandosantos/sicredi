//
//  LocaleService.swift
//  SicredEvents
//
//  Created by Fernando on 30/12/20.
//

import CoreLocation
import Contacts
import MapKit

protocol LocaleServiceProtocol {
    func getAddress(latitude: Double, longitude: Double, completion: @escaping (String?) -> Void)
    func openMaps(at placeName: String, lat: Double, lon: Double)
}

final class LocaleService: LocaleServiceProtocol {
    
    private let geocoder: CLGeocoder = CLGeocoder()
    
    func getAddress(latitude: Double, longitude: Double, completion: @escaping (String?) -> Void) {
        
        let location = getCLLocation(latitude: latitude, longitude: longitude)
        
        geocoder.reverseGeocodeLocation(location, preferredLocale: nil) { (clPlacemark: [CLPlacemark]?, error: Error?) in
            let postalAddressFormatter = CNPostalAddressFormatter()
            postalAddressFormatter.style = .mailingAddress
            
            guard let place = clPlacemark?.first, let postalAddress = place.postalAddress else {
                completion(nil)
                return
            }
            let address = postalAddressFormatter.string(from: postalAddress).replacingOccurrences(of: "\n", with: ", ")
            completion(address)
        }
    }
    
    func openMaps(at placeName: String, lat: Double, lon: Double) {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = lon
        
        let regionDistance: CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }
    
    private func getCLLocation(latitude: Double, longitude: Double) -> CLLocation {
        let lat = CLLocationDegrees(latitude)
        let lon = CLLocationDegrees(longitude)
        return CLLocation(latitude: lat, longitude: lon)
    }
}
